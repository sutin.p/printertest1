﻿using System;
using Microsoft.VisualBasic;
using System.Runtime.InteropServices;
namespace TestPrint
{

    [StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential)]
    internal struct DOC_INFO_1
    {
        [MarshalAs(UnmanagedType.LPStr)]
        public string pDocName;
        [MarshalAs(UnmanagedType.LPStr)]
        public string pOutputFile;
        [MarshalAs(UnmanagedType.LPStr)]
        public string pDatatype;
    }


    public class RawWriter
    {
        [System.Runtime.InteropServices.DllImport("kernel32.dll")]
        private static extern int GetLastError();

        [System.Runtime.InteropServices.DllImport("winspool.drv")]
        private static extern int OpenPrinterA(string pPrinterName, out int phPrinter, int pDefault);

        [System.Runtime.InteropServices.DllImport("winspool.drv")]
        private static extern int WritePrinter(int hPrinter, ref byte pBuf, int cdBuf, out int pcWritten);

        [System.Runtime.InteropServices.DllImport("winspool.drv")]
        private static extern int StartDocPrinterA(int hPrinter, int Level, ref DOC_INFO_1 pDocInfo);

        [System.Runtime.InteropServices.DllImport("winspool.drv")]
        private static extern int ClosePrinter(int hPrinter);

        [System.Runtime.InteropServices.DllImport("winspool.drv")]
        private static extern int EndDocPrinter(int hPrinter);

        [System.Runtime.InteropServices.DllImport("winspool.drv")]
        private static extern int EndPagePrinter(int hPrinter);

        public static readonly byte[] CRLF = new byte[] { 13, 10 }; // Carriage-return - Linefeed
        public static readonly byte[] CR = new byte[] { 13 }; // Carriage-return
        public static readonly byte[] LF = new byte[] { 10 }; // Linefeed
        public static readonly byte[] ESC = new byte[] { 27 }; // Escape

        public Int32 Print(string DocumentName, string PrinterName, byte[] Data) // Returns Nr of Bytes written to the printer
        {
            Int32 Retval;
            Int32 BytesWritten;
            DOC_INFO_1 MyDocInfo = new DOC_INFO_1();
            Int32 PrinterHandle;

            MyDocInfo.pDocName = DocumentName;
            MyDocInfo.pOutputFile = Constants.vbNullString;
            MyDocInfo.pDatatype = "RAW";

            // Get a handle to the printer
            Retval = OpenPrinterA(PrinterName, out PrinterHandle, 0);
            if (Retval == 0)
            {
                ClosePrinter(PrinterHandle);
                //Information.Err.Raise(GetLastError());
            }

            // Start A new document
            Retval = StartDocPrinterA(PrinterHandle, 1, ref MyDocInfo);
            if (Retval == 0)
            {
                EndDocPrinter(PrinterHandle);
                ClosePrinter(PrinterHandle);
                //Information.Err.Raise(GetLastError());
            }

            // Write The Data
            Retval = WritePrinter(PrinterHandle, ref Data[0], Data.Length, out BytesWritten);
            if (Retval == 0)
            {
                EndPagePrinter(PrinterHandle);
                EndDocPrinter(PrinterHandle);
                ClosePrinter(PrinterHandle);
                //Information.Err.Raise(GetLastError());
            }

            // Close all open items
            EndPagePrinter(PrinterHandle);
            EndDocPrinter(PrinterHandle);
            ClosePrinter(PrinterHandle);

            // return the bytes written to the printer
            return BytesWritten;
        }
    } // Rawwriter
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                //RawWriter MyWriter = new RawWriter();
                //// write some data to the printer
                //string Printer = "RICOH MPC2004ex PCL";
                //string DocumentName = "PJL Test";
                //System.Text.ASCIIEncoding AsciiConv = new System.Text.ASCIIEncoding();
                //Int32 CurentLength = 0;
                //Int32 Retval;

                //// Esc
                //byte[] UEL = AsciiConv.GetBytes("%-12345X");
                //byte[] PCLString1 = AsciiConv.GetBytes("@PJL SET OUTBIN=OPTIONALOUTBIN2");
                //// LF
                //byte[] PCLString2 = AsciiConv.GetBytes("@PJL SET FINISH=STAPLE");
                //// Esc
                //// UEL

                //byte[] Data = new byte[RawWriter.ESC.Length + UEL.Length + PCLString1.Length + RawWriter.LF.Length + PCLString2.Length + RawWriter.ESC.Length + UEL.Length - 1 + 1]; // -1, ZeroBased

                //// Fill the string ByteArray
                //RawWriter.ESC.CopyTo(Data, CurentLength); CurentLength += RawWriter.ESC.Length;
                //UEL.CopyTo(Data, CurentLength); CurentLength += UEL.Length;
                //PCLString1.CopyTo(Data, CurentLength); CurentLength += PCLString1.Length;
                //RawWriter.LF.CopyTo(Data, CurentLength); CurentLength += RawWriter.LF.Length;
                //PCLString2.CopyTo(Data, CurentLength); CurentLength += PCLString2.Length;
                //RawWriter.ESC.CopyTo(Data, CurentLength); CurentLength += RawWriter.ESC.Length;
                //UEL.CopyTo(Data, CurentLength); CurentLength += UEL.Length;

                //Retval = MyWriter.Print(DocumentName, Printer, Data);
                string inputFile = @"C:\Users\sutin.p\Downloads\pjl-example\test1.prn";

                string st1 = "Printing a string test.";

                ////st1 += "\x1b&l1X\f";

                //Printing.RawPrinterHelper.SendStringToPrinter("RICOH MPC2004ex PCL", st1);
                ///Printing.RawPrinterHelper.SendStringToPrinter("Brother HL-2270DW series GW", st1);

                //string szPrinterName = @"\\Brother HL-2270DW series GW";

                string line = (char)27 + "*c32545D";
                line += (char)27 + "*c5F";
                string setTray = "@PJL SET MEDIASOURCE = Tray1";
                string message = "test test1";
                string sendString = string.Format(@"\x1B%-12345X@PJL JOB MODE=PRINTER \r\n\ @PJL INFO PAGECOUNT \r\n\ @PJL RDYMSG DISPLAY = \""{0}\"" \r\n\ {1} x1B%-12345X\r\n", message,setTray);
                

                PrintRaw.RawFilePrint.SendStringToPrinter("\\\\lbkksutinp\\Brother12", sendString);

                //Console.WriteLine("No Error");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            
        }
    }
}
