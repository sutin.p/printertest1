﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Ghostscript.NET;
using Ghostscript.NET.Processor;
using Ghostscript.NET.Rasterizer;
using RawPrint;

namespace ConsoleAppTestPrinting
{
    public class PrnHack
    {
        public static int Main(string[] args)
        {
            try
            {
                //if (!ParseArgs(args))
                //{
                //    return -1;
                //}

                //Console.WriteLine("\nHP Display Hack");
                //Console.WriteLine("Host: {0}", args[0]);
                //Console.WriteLine("Message: {0}\n", message);

                //IPEndPoint ipEndPoint;
                //ipEndPoint = new IPEndPoint(Dns.Resolve("172.16.53.239").AddressList[0], PJL_PORT);
                //IPAddress test1 = IPAddress.Parse("172.16.53.239");
                //IPEndPoint ie = new IPEndPoint(test1, 8080);

                //Console.WriteLine("Host is {0}", ie.ToString());

                //Socket socket;
                //socket = new Socket(
                //                  AddressFamily.InterNetwork,
                //                  SocketType.Stream,
                //                  ProtocolType.Tcp
                //               );
                //socket.NoDelay = true;

                //IPAddress ip = IPAddress.Parse("172.16.53.239");
                //IPEndPoint ipep = new IPEndPoint(ip, 9100);
                //socket.Connect(ipep);

                //byte[] sendData;
                //string sendString;

                //sendString = String.Format(
                //          "\x1B%-12345X@PJL RDYMSG DISPLAY = \"{0}\"\r\n\x1B%-12345X\r\n",
                //          message
                //     );

                //sendData = Encoding.ASCII.GetBytes(sendString);

                //int result;
                //result = socket.Send(sendData, sendData.Length, 0);

                //if (result == 0)
                //{
                //    Console.WriteLine("Could not send on socket");
                //}

                //socket.Close();



                //// Create an instance of the Printer
                //IPrinter printer = new Printer();

                //// Print the file
                //string pathFile = @"C:\ProjectAware\Task\PrintingpageTest.pdf";
                //printer.PrintRawFile("Brother HL-2270DW series", pathFile, "PrintingpageTest.pdf");


                //Start();
                //PrintDocument();
                RunPjlToPrinter();

            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.WriteLine("Finished\n\n");
            return 0;

        }

        //public static void GhostscriptRasterizer()
        //{
        //    GhostscriptRasterizer rasterizer = null;
        //    Stream inputMS = File.OpenRead(@"C:\ProjectAware\Task\testSizePDF\Merging_Result.pdf");
        //    GhostscriptVersionInfo version = new GhostscriptVersionInfo(
        //                                                       new Version(0, 0, 0), @"C:\Program Files\gs\gs9.53.3\bin\gsdll64.dll",
        //                                                       string.Empty, GhostscriptLicense.GPL);
        //    using (rasterizer = new GhostscriptRasterizer())
        //    {
        //        rasterizer.Open(inputMS, version, false);

        //        for (int i = 1; i <= rasterizer.PageCount; i++)
        //        {

        //            using (MemoryStream ms = new MemoryStream())
        //            {
        //                rasterizer.CustomSwitches()
        //                Image img = rasterizer.GetPage(dpi, dpi, i);
        //                img.Save(ms, ImageFormat.Jpeg);
        //                ms.Close();

        //                AspImage newPage = new AspImage();
        //                newPage.ImageUrl = "data:image/png;base64," + Convert.ToBase64String((byte[])ms.ToArray());

        //                Document1Image.Controls.Add(newPage);
        //            }

        //        }

        //        rasterizer.Close();
        //    }
        //}

        public static void RunPjlToPrinter()
        {
            string printer_ip = "172.16.53.239";
            //string printer_ip = "172.16.48.240";

            //MemoryStream myDocument = new MemoryStream(File.ReadAllBytes(@"C:\ProjectAware\Task\testSizePDF\pageA4.pdf"));
            //var strStream = myDocument.ReadByte();
            string myString;
            using (FileStream fs = new FileStream(@"C:\Users\sutin.p\Downloads\Scoring_Guide.pcl", FileMode.Open))
            using (BinaryReader br = new BinaryReader(fs))
            {
                byte[] bin = br.ReadBytes(Convert.ToInt32(fs.Length));
                myString = Convert.ToBase64String(bin);
            }

            char command_start_character = '\u001B';
            char carriage_return = '\u000D';
            char line_feed = '\u000A';
            string lane_escape_character = string.Concat(carriage_return, line_feed); //"\r\n";
#if PJL
            string printer_process_name = "prova.TXT";
            string raw_end_of_file = command_start_character + "%–12345X" + " @PJL EOF" + lane_escape_character;
            raw_end_of_file += command_start_character + "%–12345X";
            string raw_to_print = command_start_character + "%–12345X";
            raw_to_print += " @PJL JOB NAME = \"" + printer_process_name  + "\"" + lane_escape_character;
            raw_to_print += " @PJL ENTER LANGUAGE = PCL" + lane_escape_character;
#else
            //Reset the printer and start a new PAGE
            //string raw_to_print = command_start_character + "E";
#endif
            string EOJ = @" %-12345X @PJL EOJ NAME = \""test\"" ";
            string printer_process_name = "prova";
            string raw_end_of_file = command_start_character + "%–12345X" + " @PJL EOJ" + lane_escape_character;
            //string raw_end_of_file = command_start_character + EOJ + lane_escape_character;
           // raw_end_of_file += command_start_character + "%–12345X";
            string raw_to_print = command_start_character + "%–12345X";
            raw_to_print += " @PJL JOB NAME = \"" + printer_process_name + "\"" + lane_escape_character;
            raw_to_print += " @PJL ENTER LANGUAGE = PCL" + lane_escape_character;
            raw_to_print += myString +" "+ lane_escape_character;

            //raw_to_print += " @PJL ENTER LANGUAGE = PDF" + lane_escape_character;
            //raw_to_print += EOJ + lane_escape_character;
            //raw_to_print += myString +" "+ lane_escape_character;
            //Set Printer resolution to 150 DPI
            string raw_pcl_commands = command_start_character + "&u150D" + command_start_character + "*t150R";
            //Write "Hello Wold!!" at the position 150x and 150y (the command is case sensitive).
            raw_pcl_commands += command_start_character + "*p150x150Y" + "Hello World!!!";
            //Another way to to what i've just done before
            //raw_pcl_commands += command_start_character + "*p150X" + command_start_character + "*p150Y" + "Hello World!!!";
            raw_pcl_commands += command_start_character + "*p150X" + command_start_character + "*p150Y" + "Hello World!!!";
            //I choose to use RGB colors (v6), select an rgb color (a0,b0,c0) and save it an a palette (i0) that i select (i0S). My selected color is RED (255,0,0)
            raw_pcl_commands += command_start_character + "*v6w255a0b0c0i0S";
            //Another way to to what i've just done before
            //raw_pcl_commands += command_start_character + "*v6W";
            //raw_pcl_commands += command_start_character + "*v255a0b0C" + command_start_character + "*v0i0S";
            //Draw a rectangle 300*150
            raw_pcl_commands += command_start_character + "*c300a150b5P";


            //COMANDI PCL
            raw_pcl_commands += command_start_character + "E";
            //raw_to_print += raw_pcl_commands;
#if PJL
            raw_to_print += raw_end_of_file;
#endif
            raw_to_print += raw_end_of_file;
            string setTray = "@PJL SET MEDIASOURCE = Tray1";
            string message = "test test1";
            string sendString = string.Format(@"\x1B%-12345X@PJL JOB MODE=PRINTER \r\n\ @PJL INFO PAGECOUNT \r\n\ @PJL RDYMSG DISPLAY = \""{0}\"" \r\n\ {1} x1B%-12345X\r\n", message, setTray);

            byte[] myEncoded_bytes = Encoding.UTF8.GetBytes(raw_to_print);
            //byte[] myEncoded_bytes = Encoding.Default.GetBytes(sendString);
            Socket socket = new Socket(SocketType.Stream, ProtocolType.IP);
            //Not supported on CE
            //socket.NoDelay = true;
            int printerPort = 9100;
            IPAddress ip = IPAddress.Parse(printer_ip);
            IPEndPoint ipEndPoint = new IPEndPoint(ip, printerPort);
            socket.Connect(ipEndPoint);
            socket.Send(myEncoded_bytes);
            socket.Close();

            //Socket pSocket = new Socket(SocketType.Stream, ProtocolType.IP);

            //pSocket.SendTimeout = 1500;
            //// Connect to the specified ip address and port
            //pSocket.Connect(printer_ip, 9100);
            //List<string> myText = new List<string>() { "Line1", "Line2" };

            //List<byte> outputList = new List<byte>();
            //foreach (string txt in myText)
            //{
            //    outputList.Add(0x9D);
            //    // Convert the strings to list of bytes
            //    outputList.AddRange(Encoding.UTF8.GetBytes(txt));
            //    // Add ECS/POS Print and line feed command

            //    outputList.Add(0x8E);
            //    outputList.Add(0x0A);
            //}
            //// Send the command to the printer
            ////outputList.AddRange(Encoding.UTF8.GetBytes(raw_to_print));
            //pSocket.Send(outputList.ToArray());

            //// Close the connection once done
            //pSocket.Close();
        }
       
        public static void Start()
        {
            // YOU NEED TO HAVE ADMINISTRATOR RIGHTS TO RUN THIS CODE

            //string printerName = "Brother HL-2270DW series";
            
            string printerName = "RICOH MPC2004ex PCL";
            string inputFile = @"C:\ProjectAware\Task\testSizePDF\Merging_Result.pdf";

            const string psScript = @"-q -c '<< /InputAttributes currentpagedevice /InputAttributes get dup { pop 1 index exch null put } forall dup 1 << /PageSize [595 420] >> put dup 2 << /PageSize [842 595] >> put >> setpagedevice' -f in.ps";
            //const string psScript = @"-q -c \""<< /PageSize [595 420] /MediaPosition 1 >> setpagedevice\"" -c \""quit\"" ";
            //const string psScript = @"-empty -dPrinted -dBATCH -dSAFER -dNOPAUSE -dNumCopies=1 -sDEVICE=mswinpr2 -sOutputFile=%printer%HP DeskJet 2130 series -q -c \""<< /PageSize [612 792] /MediaPosition 1 >>setpagedevice\"" -c \""quit\"" \""C:\ProjectAware\Task\testSizePDF\Merging_Result.pdf\""";
            //GhostscriptVersionInfo gvi = new GhostscriptVersionInfo(new Version(0, 0, 0), @"C:\Program Files\gs\gs9.53.3\bin\gsdll64.dll", string.Empty, GhostscriptLicense.GPL);
            using (GhostscriptProcessor processor = new GhostscriptProcessor())
            {
                List<string> switches = new List<string>();
                switches.Add("-empty");
                switches.Add("-dPrinted");
                switches.Add("-dBATCH");
                switches.Add("-dSAFER");
                switches.Add("-dNOPAUSE");
                //switches.Add("-dFIXEDMEDIA");
                //switches.Add("-dDEVICEWIDTHPOINTS=674");
                //switches.Add("-dDEVICEHEIGHTPOINTS=912");
                //switches.Add("-dPDFFitPage");
                switches.Add("-dNumCopies=1");
                switches.Add("-sDEVICE=mswinpr2");
                
                //switches.Add(@"-q -c\""<</PageSize [595 842] /Orientation 3>> setpagedevice\"" ");
                switches.Add("-sOutputFile=%printer%" + printerName);
                switches.Add(psScript);
                switches.Add("\""+inputFile+"\"");

                processor.StartProcessing(switches.ToArray(), null);
            }
        }



        public static void PrintDocument()
        {
            //if (!File.Exists(fs.FullyQualifiedName)) return;
            
            try
            {

                const string gsPrintExecutable = @"C:\Program Files\Ghostgum\gsview\gsprint.exe";
                const string gsExecutable = @"C:\Program Files\gs\gs9.53.3\bin\gswin64c.exe";

                string pdfPath = @"C:\ProjectAware\Task\Scoring_Guide.pdf";
                string printerName = "Brother HL-2270DW series";


                string processArgs = string.Format("-ghostscript \"{0}\" -copies=1 -all -printer \"{1}\" \"{2}\"", gsExecutable, printerName, pdfPath);

                var gsProcessInfo = new ProcessStartInfo
                {
                    WindowStyle = ProcessWindowStyle.Hidden,
                    FileName = gsPrintExecutable,
                    Arguments = processArgs
                };
                using (var gsProcess = Process.Start(gsProcessInfo))
                {

                    gsProcess.WaitForExit();

                }

            }
            catch(Exception ex)
            {
                var resutl = ex.Message;
            }

         }



        protected static bool ParseArgs(string[] args)
        {
            if (args.Length != 2)
            {
                Console.WriteLine(
                          "HP Display Hack: " +
                          "hphack printername \"message\" "
                    );
                return false;
            }

            if (args[1].Length > 16)
            {
                Console.WriteLine("Message must be <= 16 characters");
                return false;
            }

            if (args[1].CompareTo("random") == 0)
            {
                message = GetRandomMessage();
            }
            else
            {
                message = args[1];
            }

            return true;
        }


        public static string GetRandomMessage()
        {
            string[] Messages = {
                             "BUZZ OFF",
                             "TOUCH ME",
                             "STEP AWAY",
                             "SET TO STUN",
                             "SCORE = 3413",
                             "PAT EATS MICE",
                             "FEED ME",
                             "GO AWAY",
                             "NEED MORE SPACE",
                             "POUR ME A DRINK",
                             "IN DISTRESS",
                             "NICE SHIRT",
                             "GO AWAY",
                             "NO PRINT FOR YOU",
                             "RADIATION LEAK",
                             "HANDS UP",
                             "PRESS MY BUTTON",
                             "TAKE ME HOME",
                             "LOOKS LIKE RAIN",
                             "HELLO WORLD",
                             "NICE HAIR",
                             "NEED A MINT?",
                             "BE GENTLE",
                             "BE KIND",
                             "INSERT DISK",
                             "BUY ME LUNCH",
                             "DONT STOP",
                             "COME CLOSER",
                             "TAKE A BREAK",
                             "INSERT QUARTER",
                             "BLACK SABBATH"
      };


            Random r = new Random();
            return Messages[r.Next() % Messages.Length];
        }

        protected const int PJL_PORT = 9100;
        protected static string message = "NO MESSAGE";

    }

}
