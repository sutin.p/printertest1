﻿using Ghostscript.NET;
using Ghostscript.NET.Processor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    [StructLayout(LayoutKind.Sequential)]
    public struct DOCINFO
    {
        [MarshalAs(UnmanagedType.LPWStr)]
        public string pDocName;
        [MarshalAs(UnmanagedType.LPWStr)]
        public string pOutputFile;
        [MarshalAs(UnmanagedType.LPWStr)]
        public string pDataType;
    }
    public class PrintDirect
    {
        [DllImport("winspool.drv", CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern long OpenPrinter(string pPrinterName, ref IntPtr phPrinter, int pDefault);

        [DllImport("winspool.drv", CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern long StartDocPrinter(IntPtr hPrinter, int Level, ref DOCINFO pDocInfo);

        [DllImport("winspool.drv", CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern long StartPagePrinter(IntPtr hPrinter);

        [DllImport("winspool.drv", CharSet = CharSet.Ansi, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern long WritePrinter(IntPtr hPrinter, string data, int buf, ref int pcWritten);

        [DllImport("winspool.drv", CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern long EndPagePrinter(IntPtr hPrinter);

        [DllImport("winspool.drv", CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern long EndDocPrinter(IntPtr hPrinter);

        [DllImport("winspool.drv", CharSet = CharSet.Unicode, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern long ClosePrinter(IntPtr hPrinter);
    }
    class Program
    {

        public static void Print(String printerAddress, String text, String documentName)
        {
            IntPtr printer = new IntPtr();

            // A pointer to a value that receives the number of bytes of data that were written to the printer.
            int pcWritten = 0;

            DOCINFO docInfo = new DOCINFO();
            docInfo.pDocName = documentName;
            docInfo.pDataType = "RAW";

            PrintDirect.OpenPrinter(printerAddress, ref printer, 0);
            PrintDirect.StartDocPrinter(printer, 1, ref docInfo);
            PrintDirect.StartPagePrinter(printer);

            try
            {
                PrintDirect.WritePrinter(printer, text, text.Length, ref pcWritten);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            PrintDirect.EndPagePrinter(printer);
            PrintDirect.EndDocPrinter(printer);
            PrintDirect.ClosePrinter(printer);
        }
        static void Main(string[] args)
        {
            try
            {
                string printerName = "HP DeskJet 2130 series";
                string inputFile = @"C:\ProjectAware\Task\testSizePDF\pageA4.pdf";

                ////const string psScript = @"-q -c '<< /InputAttributes currentpagedevice /InputAttributes get dup { pop 1 index exch null put } forall dup 1 << /PageSize [595 420] >> put dup 2 << /PageSize [842 595] >> put >> setpagedevice' -f in.ps";
                //const string psScript = @"-q -c \""<< /PageSize [842 595] /MediaPosition 1 >> setpagedevice\"" -c \""quit\"" ";
                ////const string psScript = @"-empty -dPrinted -dBATCH -dSAFER -dNOPAUSE -dNumCopies=1 -sDEVICE=mswinpr2 -sOutputFile=%printer%HP DeskJet 2130 series -q -c \""<< /PageSize [612 792] /MediaPosition 1 >>setpagedevice\"" -c \""quit\"" \""C:\ProjectAware\Task\testSizePDF\Merging_Result.pdf\""";
                //GhostscriptVersionInfo gvi = new GhostscriptVersionInfo(new Version(0, 0, 0), Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "gsdll32.dll"), string.Empty, GhostscriptLicense.GPL);
                //GhostscriptProcessor processor = new GhostscriptProcessor(gvi);

                //List<string> switches = new List<string>();
                //switches.Add("-empty");
                //switches.Add("-dPrinted");
                //switches.Add("-dBATCH");
                //switches.Add("-dSAFER");
                //switches.Add("-dNOPAUSE");
                //switches.Add("-dNORANGEPAGESIZE");
                ////switches.Add("-dFIXEDMEDIA");
                ////switches.Add("-dDEVICEWIDTHPOINTS=674");
                ////switches.Add("-dDEVICEHEIGHTPOINTS=912");
                ////switches.Add("-dPDFFitPage");
                //switches.Add("-dNumCopies=1");
                //switches.Add("-sDEVICE=mswinpr2");
                ////switches.Add(psScript);            
                //switches.Add("-sOutputFile=%printer%" + printerName);
                ////switches.Add("-c");
                //switches.Add(@"-c \"" <</PSDocOptions (<</ Orientation 3 >> setpagedevice)>> setdistillerparams\"" -f");
                ////switches.Add("-dAutoRotatePages=/None");
                ////switches.Add(@"-q -c \""C:\Users\sutin.p\Downloads\lpl.ps\"" -c \""quit\""");
                //switches.Add("-f");
                //switches.Add("\"" + inputFile + "\"");

                //processor.StartProcessing(switches.ToArray(), null);

                String printerAddress = "RICOH MPC2004ex PCL";
                String documentName = "My document";
                String documentText = "This is an example of printing directly to a printer.";
                Print(printerAddress, documentText, documentName);
            }
            catch(Exception ex)
            {
                var message = ex.Message;
            }
            
            
        }
    }
}
